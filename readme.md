﻿# Письмо
﻿
﻿﻿[<img src="https://img.shields.io/badge/%D0%9F%D0%B0%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B8%D0%BA%20IV-%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%BD%D0%B8%D0%BA-brightgreen.svg">](http://forum.ifiction.ru/viewtopic.php?id=2337&p=1)

Небольшая интерактивная история про человеческие отношения, использующая «эпистолярную механику». 

* __[Играть онлайн](https://johnbrown90210.gitlab.io/The_Letter_online)__
* [Видеопрохождение от Адженты](https://www.youtube.com/watch?v=njHa3gcat4g&list=PLitEPKHV7FSdgZ_mnX6aj7BGnvCDHNGHg&index=2)
* [Обзор от Антона Жучкова](https://if.zhuchkovs.com/2018/04/p4-pismo-johnbrown/)

### Системные требования

* Большинство современных браузеров

### Запуск

Запустите файл **index.html**

### ЧАВО

* **Как сохраняться?**

К сожалению, встроенные в движок Dedalus функции сохранения/восстановления работают некорректно с переменными. Пришлось их отключить, но игра достаточно короткая, чтобы этими функциями можно было пренебречь.